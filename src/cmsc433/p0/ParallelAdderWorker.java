package cmsc433.p0;

import java.util.LinkedList;

class ParallelAdderWorker extends Thread
{
    private LinkedList<Integer> numbers;
    
    private Integer partialSum = 0;

    public ParallelAdderWorker(LinkedList<Integer> numbers) 
    {
        this.numbers = numbers;
    }
    
    public Integer getPartialSum()
    {
        return 0;
    }
    
    @Override
    public void run()
    {
        while (!this.numbers.isEmpty())
        {
            Integer n = this.numbers.poll();
            
            if (n != null)
            {
                this.partialSum += n;
            }
        }
    }
}
