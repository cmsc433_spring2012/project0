package cmsc433.p0;

import java.util.LinkedList;

public class ParallelAdder
{
    private ParallelAdderWorker[] workers;

    public ParallelAdder(int threadCount)
    {
        this.workers = new ParallelAdderWorker[threadCount];
    }

    public int sum(LinkedList<Integer> numbers) throws InterruptedException, Exception
    {
        for (int i = 0; i < this.workers.length; ++i)
        {
            this.workers[i] = new ParallelAdderWorker(numbers);
            this.workers[i].start();
        }

        int result = 0;
        for (int i = 0; i < this.workers.length; ++i)
        {
            this.workers[i].join(2000);
            if (this.workers[i].isAlive()) { throw new Exception("One of the workers is blocked."); }

            result += this.workers[i].getPartialSum();
        }
        
        return result;
    }
}
